﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace PDFMerge
{
    class Program
    {
        static void Main(string[] args)
        {
            //only allow a minimum of 3 parameters
            if (args.Length == 0 || args.Length > 3 || (args.Length < 3 && args[0] != "/?"))
            {

                Console.WriteLine("Wrong number of parametes. Please type /? to see usage settings."); // Check for null array
                return;

            }
            else
            {
                if (args[0] == "/?")
                {
                    Console.WriteLine("USAGE : ");
                    Console.WriteLine("PDFMerge [PDFFileName] [SourceFolder] [FilePattern]");
                    Console.WriteLine("[PDFFileName] : Name of the PDF file to create including file path. ");
                    Console.WriteLine("[SourceFolder] : Location of the files to include in the resulting PDF file. ");
                    Console.WriteLine("[FilePattern] : File Pattern for the files in the Souce Folder. ");


                    Console.WriteLine("Example 1: PDFMerge C:\\Test\test1.pdf c:\\TestFiles Test*.PDF\n");

                    Console.WriteLine("Authors :Rodolfo Ramos/John Cortenbach \n\n");
                    return;
                }
            }
            string PDFFileName = string.Empty;
            string SourceFolder = string.Empty;
            string FilePattern = string.Empty;


            PDFFileName = args[0];
            SourceFolder = args[1];
            FilePattern = args[2];
            //Check if folders exist
            string destpath= string.Empty;
            string sourcepath = string.Empty;
            destpath=Path.GetDirectoryName(PDFFileName);
            
            //Check if paths are valid
            if (! Directory.Exists(destpath))
            {
                Console.WriteLine("Invalid destination path!");
                return;
            }

            if (!Directory.Exists(SourceFolder))
            {
                Console.WriteLine("Invalid source folder path!");
                return;
            }


            //Make sure the file extension is .PDF, if there is no .PDF extension, add the extension
            if (!PDFFileName.ToString().ToLower().Contains(".pdf"))
            {
                PDFFileName += ".pdf";
            }
            try
            {
                FileProcessor fp = new FileProcessor();
                fp.MergeFiles(PDFFileName, SourceFolder, FilePattern);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error on Main Module :" + e.Message.ToString());
            }
        }
    }
}
