﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using HiQPdf;

namespace PDFMerge
{
    class FileProcessor
    {
        public void MergeFiles(string pdfresult, string sourcedirectory, string filepattern)
        {
            try
            {
                PdfDocument mainDoc = new PdfDocument();
                mainDoc.SerialNumber = "fzcWLi8b-GTMWHQ0e-DQZHUU9f-Tl9NX0dL-SF9MTlFO-TVFGRkZG";

                string fileName = string.Empty;
                string pdftoadd = string.Empty;
                //Scan Source Directory for all PDF files
                if (Directory.Exists(sourcedirectory))
                {
                    string[] files = Directory.GetFiles(sourcedirectory, filepattern);

                    int docnum = 0;

                    foreach (string pdfFile in files)
                    {

                        // Use static Path methods to extract only the file name from the path.
                        fileName = System.IO.Path.GetFileName(pdfFile);
                        pdftoadd = System.IO.Path.Combine(sourcedirectory, fileName);

                        FileStream pdfStream = new FileStream(pdftoadd, FileMode.Open, FileAccess.Read, FileShare.Read);
                        PdfDocument documenttoadd = PdfDocument.FromStream(pdfStream);


                        mainDoc.AddDocument(documenttoadd);

                    }
                    mainDoc.WriteToFile(pdfresult);
                }
                mainDoc.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error on MergeFiles Method :" + e.Message.ToString());
            }
        


            
        }

    }
}
